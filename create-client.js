const DYNAMODB = require("aws-sdk/clients/dynamodb");
const dynamodb = new DYNAMODB({ region: "us-east-1" });

const SNS = require("aws-sdk/clients/sns");
const sns = new SNS({ region: "us-east-1" });

const TABLE_NAME = "client";

const TOPIC_ARN_NEW_CLIENT = "arn:aws:sns:us-east-1:191644627630:new-client";

function age(birthday) {
    birthday = new Date(birthday);
    return ((new Date().getTime() - birthday.getTime()) / 31536000000).toFixed(0);
}

//Cargar clientes con nombre, apellido y fecha de nacimiento. Solo debe permitir mayores de edad hasta 65 años
exports.handler = async (event) => {

    const body = event;
    if (body.dni && body.name && body.lastName && age(body.birthdayDate) >= 18 && age(body.birthdayDate) <= 65) {
        const params = {
            Item: {
                "dni": {
                    S: body.dni
                },
                "name": {
                    S: body.name
                },
                "lastName": {
                    S: body.lastName
                },
                "birthdayDate": {
                    S: body.birthdayDate
                }
            },
            TableName: TABLE_NAME
        }

        const paramsSNS = {
            Message: JSON.stringify(event),
            Subject: "client-created",
            TopicArn: TOPIC_ARN_NEW_CLIENT
        }
        try {
            const dbResult = await dynamodb.putItem(params).promise();
            console.log(dbResult)
            const snsResult = await sns.publish(paramsSNS).promise();
            console.log(snsResult)
            return { statusCode: 200 };
        } catch (error) {
            console.log("error");
            console.log(error);
        }

    }


}