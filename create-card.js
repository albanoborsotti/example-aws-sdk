const DYNAMODB = require("aws-sdk/clients/dynamodb");
const dynamodb = new DYNAMODB({ region: "us-east-1" });

const TABLE_NAME = "client";
//Que se le asigne un regalo de cumpleaños según la estación en la que cae, variando entre buzo si es otoño, sweater si es invierno,
//camisa si es primavera y remera si es verano.

function age(birthday) {
    birthday = new Date(birthday);
    return ((new Date().getTime() - birthday.getTime()) / 31536000000).toFixed(0);
}

function randomNumber() {
    return (Math.floor(Math.random() * 10000) + 10000).toString().substring(1);
}

function getNumberCard() {
    return randomNumber() + "-" + randomNumber() + "-" + randomNumber() + "-" + randomNumber();
}

function getExpirationDate() {
    return String(Math.floor(Math.random() * (12 - 1 + 1)) + 1).padStart(2, '0') + "/" + Math.floor((Math.random() * (36 - 25) + 25));
}

function getCCV() {
    return (Math.floor(Math.random() * 1000) + 1000).toString().substring(1);
}


//Otorgarle una tarjeta de crédito (Classic si es menor a 45 y Gold si es mayor) generando número, vencimiento y código de seguridad.
exports.handler = async (event) => {

    const queue = event.Records.map((record) => record.body)

    for (const item of queue) {
        const itemParsed = JSON.parse(item)
        const body = JSON.parse(itemParsed.Message)


        let creditCard = {
            number: getNumberCard(),
            expirationDate: getExpirationDate(),
            ccv: getCCV(),
            type: age(body.birthdayDate) <= 45 ? 'classic' : 'gold'
        }


        var params = {
            ExpressionAttributeNames: {
                "#CD": "creditCard"
            },
            ExpressionAttributeValues: {
                ":c": {
                    S: JSON.stringify(creditCard)
                }
            },
            Key: {
                "dni": {
                    S: body.dni
                }
            },
            ReturnValues: "ALL_NEW",
            TableName: TABLE_NAME,
            UpdateExpression: "SET #CD = :c"
        };

        try {
            const resDB = dynamodb.updateItem(params).promise();
            console.log(resDB);
            return { statusCode: 200 };
        } catch (error) {
            console.log("Error", error);
        }

    }

}