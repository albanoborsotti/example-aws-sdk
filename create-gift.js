const DYNAMODB = require("aws-sdk/clients/dynamodb");
const dynamodb = new DYNAMODB({ region: "us-east-1" });

const TABLE_NAME = "client";

//Que se le asigne un regalo de cumpleaños según la estación en la que cae, variando entre buzo si es otoño, sweater si es invierno,
//camisa si es primavera y remera si es verano.

function getGift(dateOfBirdth) {
    const brd = new Date(dateOfBirdth)
    const month = brd.getMonth() + 1;
    const day = brd.getDate();
    if ((month <= 3 && day <= 20) || (month === 12 && day >= 21)) {
        return 'T-shirt';
    } else if (month <= 6 && day < 21) {
        return 'buzo';
    } else if (month <= 9 && day < 21) {
        return 'sweater'
    } else {
        return 'shirt'
    }
}

exports.handler = async (event) => {
    const queue = event.Records.map((record) => record.body)

    for (const item of queue) {
        const itemParsed = JSON.parse(item)
        const body = JSON.parse(itemParsed.Message)

        var params = {
            ExpressionAttributeNames: {
                "#GF": "gift"
            },
            ExpressionAttributeValues: {
                ":g": {
                    S: getGift(body.birthdayDate)
                }
            },
            Key: {
                "dni": {
                    S: body.dni
                }
            },
            ReturnValues: "ALL_NEW",
            TableName: TABLE_NAME,
            UpdateExpression: "SET #GF = :g"
        };

        try {
            const resDB = await dynamodb.updateItem(params).promise();
            console.log(resDB);
            return { statusCode: 200 };
        } catch (error) {
            console.log("Error", error);
        }

    }
}